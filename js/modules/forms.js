var forms = (function () {
	var emailRe = /^([a-z0-9_\.\-\+]+)@([\da-z\.\-]+)\.([a-z\.]{2,6})$/i;
	var phoneRe = /^[- +()0-9]+$/i;
	return {
		init: function () {
			forms.phoneMask();
			forms.createNumberType();
			forms.placeholderMove();
			forms.inputChange();
			forms.submit();
			forms.placeholderInit();
			forms.processing();
		},

		/**
		 * Добавляет маску на инпуты с типом phone
		 */
		phoneMask: function () {
			var $phoneInput = $(".js-phone");
			if ($phoneInput.length) {
				$phoneInput.mask("+7 (999) 999-99-99", {autoclear:false});
			}
		},

		/**
		 * Инициализирует инпуты с типом Number
		 */
		createNumberType: function () {
			$('.js-form__number').each(function () {
				var $input = $(this),
					$plus = $input.siblings('.js-form__number-plus'),
					$minus = $input.siblings('.js-form__number-minus'),
					min = parseFloat($input.attr('min')),
					max = parseFloat($input.attr('max')),
					step = $input.attr('step') ? parseFloat($input.attr('step')) : 1,
					oldValue = parseFloat($input.val());
				$input.data('start-value', $input.val());
				$input.data('prev-value', $input.val());
				if (oldValue >= max) {
					$input.val(max);
					$plus.addClass('maxed');
				} else if (oldValue <= min) {
					$input.val(min);
					$minus.addClass('maxed');
				}
				$input.change(function () {
					oldValue = parseFloat($(this).val());
					if (oldValue >= max) {
						$input.val(max);
						$plus.addClass('maxed');
						$minus.removeClass('maxed');
						oldValue = max;

					} else if (oldValue <= min) {
						$input.val(min);
						$minus.addClass('maxed');
						$plus.removeClass('maxed');
						oldValue = min;
					}
					oldValue = ( $(this).val() - $(this).data('prev-value') ) ;
					$(this).data('prev-value', $(this).val());
				});
				$plus.click(function () {
					oldValue = parseFloat($input.val());
					$minus.removeClass('maxed');
					if (oldValue >= max) {
						$input.val(oldValue);
						$plus.addClass('maxed');
					} else {
						$plus.removeClass('maxed');
						$input.val(oldValue + step);
						$input.data('prev-value', +$input.data('prev-value') + step );
						if (oldValue + step >= max) {
							$plus.addClass('maxed');
						}
						$input.trigger('change');
					}
				});
				$minus.click(function () {
					oldValue = parseFloat($input.val());
					$plus.removeClass('maxed');
					if (oldValue <= min) {
						$input.val(oldValue);
						$minus.addClass('maxed');
					} else {
						$minus.removeClass('maxed');
						$input.val(oldValue - step);
						$input.data('prev-value', +$input.data('prev-value') - step );
						if (oldValue - step <= min) {
							$minus.addClass('maxed');
						}
						$input.trigger('change');
					}
				});
			});
		},

		placeholderInit: function() {
            $( document ).ready(function() {
				$('.js-input').each(function() {
					var $input = $(this);

					if ($input.val() !== '') {
						$input.addClass('active');
					} else {
						$input.removeClass('active valid');
					}
				});
			});
		},

		/**
		 * Сдвигает плейсхолдер при фокусе добавляя класс
		 */
		placeholderMove: function () {
			$('.js-input').on('keyup keypress blur focus change', function(e) {
				if ($(this).val() !== '') {
					$(this).addClass('active');
				} else {
					$(this).removeClass('active valid');
				}
			});
		},

		/**
		 * Убирает класс ошибки при фокусе и
		 * инициализирует валидацию на событие change
		 */
		inputChange: function () {
			var $inputs = $('.js-input');
			$inputs.focus(function () {
				$(this).removeClass('error');
			});
			$inputs.on('change', function () {
 				if ($(this).hasClass('js-required') || $(this).hasClass('js-validate')) {
					forms.inputValidate($(this));
				}
			});
		},

		/**
		 * Валидация инпута
		 */
		inputValidate: function ($element) {
			//console.log($element.attr('name'));
			var value = $element.val(),
				valid = false,
				$parent = $($element).parent('.form__text-input-b, .form__checkbox-input-b');
			if (value.length === 0 && !$element.hasClass('js-required')) {
				//console.log($element);
				$element.removeClass('error').removeClass('valid');
				if ($parent.length) {
					$parent.removeClass('valid');
				}
				return;
			}
			switch ($element.attr('type')) {
				case 'email':
					valid = emailRe.test(value);
					break;
				case 'tel':
					valid = phoneRe.test(value);
					break;
				case 'checkbox':
					valid = $element.prop('checked');
					break;
				default:
					valid = (value !== "");
					break;
			}
			if (valid) {
				$element.removeClass('error').addClass('valid');
				if ($parent.length) {
					$parent.addClass('valid').removeClass('error');
				}
			} else if ($element.hasClass('js-required') || $element.hasClass('js-validate')) {
				$element.removeClass('valid').addClass('error');
				if ($parent.length) {
					//console.log('parent error')
					$parent.removeClass('valid').addClass('error');
				}
			}
		},

		/**
		 * Обработать ответ страницы
		 */
		isErorrsReturnAndRender:  function($form, response) {
			var hasErorrs = false,
                $input = $form.find('.js-input'),
                $error = $form.find('.js-form__error');

            if(response.success){
                hasErorrs = false;
                $input.removeClass('error').addClass('valid');
                $error.removeClass('active');
            } else{
                $input.addClass('error').removeClass('valid');
                $error.addClass('active').text(response.error);
                hasErorrs = true;
            }
			return hasErorrs;
		},

		/**
		 * Отправка формы
		 */
		submit: function () {
			$('.js-form').submit(function (event) {
				event.preventDefault();
				var $form = $(this),
				dataCorrect = true,
				method,
				data;
				$form.find('.js-input.js-required').each(function () { //:visible
					forms.inputValidate($(this));
				});
				$form.find('.js-required').each(function () {
					if ($(this).hasClass('error')) {
						dataCorrect = false;
					}
				});
				if (dataCorrect) {
                    $form.trigger('form_start');
                    data = $form.serializeArray();
                    method = $form.attr('method');
                    $.ajax({
                        type: method ? method : 'POST',
                        url: $form.attr('action'),
                        data: data,
                        dataType: "json",
						success: function(resp) {
							if(!forms.isErorrsReturnAndRender($form, resp)) {
								$form.trigger('form_success', resp);
							}
							else {
								$form.trigger('form_error', resp);
							}
						},
						error: function(resp) {
							forms.showErrorMessage($form, [$form.data('error-msg')]);
							$form.trigger('form_error', resp);
						}
					});
				} else {
					return false;
				}
			});
		},

		/**
		 * Вывести сообщение об ошибке в форме
		 */
		showErrorMessage: function($element, $messages) {
			for(var $key in $messages) {
				var $error = $('<div class="form__send-fail" style="opacity: 0">' + $messages[$key] + '</div>');
				//if ($element.data('error-msg')) {
					$element.append($error);
					$error.fadeTo("fast" , 1, function() {
						setTimeout(function(){
							$error.first().fadeOut("fast", function() {
								$element.find('.form__send-fail').remove();
							});
						}, 1500);
					});
				//}
			}
		},

		processing: function() {
			$('.js-form').on('form_start', function() {
				$(this).addClass('processing');
			});

			/*$('.js-form').on('form_success form_error', function() {
				$(this).removeClass('processing');
			})*/
            $('.js-form').on('form_success', function() {
                $('.js-form__content').hide();
                $('.js-form__success').show();
                $.magnificPopup.open({
                    items: {
                        src: '#modal', // can be a HTML string, jQuery object, or CSS selector
                        type: 'inline'
                    }
                })
            });

            $('.js-reset-form').click(function (e) {
                console.log('reset');
                e.preventDefault();
                $.each( $('.js-form'), function( key, $form ) {
                    $form.reset();
                });
                $('.js-form__success').hide();
                $('.js-form__content').show();
            });

		}
	}
})();