"use strict";

//= vendor/jquery.min.js
//= vendor/inputmask/inputmask.js
//= ../node_modules/smooth-scroll/dist/smooth-scroll.polyfills.js
//= ../node_modules/magnific-popup/dist/jquery.magnific-popup.js
//= ../node_modules/slick-carousel/slick/slick.js
//= modules/forms.js

var mobile = (function () {
    return {
        init: function () {
            forms.init();
            mobile.toggle();
            mobile.map();
            mobile.smoothScroll();
            mobile.initCarousels();
            mobile.initMenu();
            mobile.popupClose();
        },

        toggle: function () {
            $(".js-accordion__toggle").click(function() {
                var $toggler = $(this).find(".js-accordion__toggle-icon"),
                    $content = $(this).next();
                $('.js-accordion__toggle-icon.arrow-up').not($toggler).toggleClass("arrow-down arrow-up");
                $('.js-accordion__content.open').not($content).removeClass('open');
                $toggler.toggleClass("arrow-down arrow-up");
                $content.toggleClass("open");
            })
        },

        map: function () {
            ymaps.ready(function () {
                var myMap = new ymaps.Map('map', {
                    center: [59.894282, 30.483080],
                    zoom: 10
                }, {
                    searchControlProvider: 'yandex#search'
                });
                var wurth= [];
                wurth[1] = new ymaps.Placemark([59.845517, 30.316811], {
                    hintContent: 'м. Московская, ул. Варшавская 120, корп.1',
                    balloonContent: 'м. Московская, ул. Варшавская 120, корп.1'
                }, {
                    iconLayout: 'default#image',
                    iconImageHref: 'img/map-marker.png',
                    iconImageSize: [56, 67],
                    iconImageOffset: [-5, -38]
                });
                wurth[2] = new ymaps.Placemark([59.995476, 30.234526], {
                    hintContent: 'м. Старая Деревня, Планерная ул., д. 15Б, торговый комплекс «Гранд Авто» ',
                    balloonContent: 'м. Старая Деревня, Планерная ул., д. 15Б, торговый комплекс «Гранд Авто» '
                }, {

                    iconLayout: 'default#image',
                    iconImageHref: 'img/map-marker.png',
                    iconImageSize: [56, 67],
                    iconImageOffset: [-5, -38]
                });
                wurth[3] = new ymaps.Placemark([59.984503, 30.365886], {
                    hintContent: ' м. Лесная, пр. Маршала Блюхера, д. 1 ',
                    balloonContent: ' м. Лесная, пр. Маршала Блюхера, д. 1 '
                }, {
                    iconLayout: 'default#image',
                    iconImageHref: 'img/map-marker.png',
                    iconImageSize: [56, 67],
                    iconImageOffset: [-5, -38]
                });
                wurth[4] = new ymaps.Placemark([59.893407, 30.439386], {
                    hintContent: ' м. Елизаровская, пр. Обуховской Обороны, д. 86Н ',
                    balloonContent: ' м. Елизаровская, пр. Обуховской Обороны, д. 86Н '
                }, {
                    iconLayout: 'default#image',
                    iconImageHref: 'img/map-marker.png',
                    iconImageSize: [56, 67],
                    iconImageOffset: [-5, -38]
                });
                wurth[5] = new ymaps.Placemark([59.807052, 30.162229], {
                    hintContent: 'Таллинское ш., д. 159 ',
                    balloonContent: 'Таллинское ш., д. 159 '
                }, {
                    iconLayout: 'default#image',
                    iconImageHref: 'img/map-marker.png',
                    iconImageSize: [56, 67],
                    iconImageOffset: [-5, -38]
                });
                wurth[6] = new ymaps.Placemark([64.536531, 40.529884], {
                    hintContent: 'проспект Ломоносова, 121',
                    balloonContent: 'проспект Ломоносова, 121 '
                }, {
                    iconLayout: 'default#image',
                    iconImageHref: 'img/map-marker.png',
                    iconImageSize: [56, 67],
                    iconImageOffset: [-5, -38]
                });
                wurth[7] = new ymaps.Placemark([58.394421, 33.937534], {
                    hintContent: 'Советская улица, 131',
                    balloonContent: 'Советская улица, 131'
                }, {
                    iconLayout: 'default#image',
                    iconImageHref: 'img/map-marker.png',
                    iconImageSize: [56, 67],
                    iconImageOffset: [-5, -38]
                });
                wurth[8] = new ymaps.Placemark([58.542978, 31.265486], {
                    hintContent: 'Большая Санкт-Петербургская улица, 56',
                    balloonContent: 'Большая Санкт-Петербургская улица, 56'
                }, {
                    iconLayout: 'default#image',
                    iconImageHref: 'img/map-marker.png',
                    iconImageSize: [56, 67],
                    iconImageOffset: [-5, -38]
                });
                wurth[9] = new ymaps.Placemark([59.199896, 39.859902], {
                    hintContent: 'г. Вологда, Пошехонское шоссе, 18кС1 (ТК "Элемент") ',
                    balloonContent: 'г. Вологда, Пошехонское шоссе, 18кС1 (ТК "Элемент") '
                }, {
                    iconLayout: 'default#image',
                    iconImageHref: 'img/map-marker.png',
                    iconImageSize: [56, 67],
                    iconImageOffset: [-5, -38]
                });
                wurth[10] = new ymaps.Placemark([54.710688, 20.440320], {
                    hintContent: 'проспект Победы, 145',
                    balloonContent: 'проспект Победы, 145'
                }, {
                    iconLayout: 'default#image',
                    iconImageHref: 'img/map-marker.png',
                    iconImageSize: [56, 67],
                    iconImageOffset: [-5, -38]
                });
                wurth[11] = new ymaps.Placemark([58.620699, 49.654153], {
                    hintContent: 'Октябрьский проспект, 79',
                    balloonContent: 'Октябрьский проспект, 79 '
                }, {
                    iconLayout: 'default#image',
                    iconImageHref: 'img/map-marker.png',
                    iconImageSize: [56, 67],
                    iconImageOffset: [-5, -38]
                });
                wurth[12] = new ymaps.Placemark([64.611830, 30.643987], {
                    hintContent: 'шоссе Горняков, 153',
                    balloonContent: 'шоссе Горняков, 153'
                }, {
                    iconLayout: 'default#image',
                    iconImageHref: 'img/map-marker.png',
                    iconImageSize: [56, 67],
                    iconImageOffset: [-5, -38]
                });
                wurth[13] = new ymaps.Placemark([68.921315, 33.115863], {
                    hintContent: 'г. Мурманск, Кольский пр., д.134 (ТРЦ "Форум", уровень В) ',
                    balloonContent: 'г. Мурманск, Кольский пр., д.134 (ТРЦ "Форум", уровень В) '
                }, {
                    iconLayout: 'default#image',
                    iconImageHref: 'img/map-marker.png',
                    iconImageSize: [56, 67],
                    iconImageOffset: [-5, -38]
                });
                wurth[14] = new ymaps.Placemark([68.138475, 33.277829], {
                    hintContent: 'г. Оленегорск, Ленинградский пр-т, д. 4',
                    balloonContent: 'г. Оленегорск, Ленинградский пр-т, д. 4'
                }, {
                    iconLayout: 'default#image',
                    iconImageHref: 'img/map-marker.png',
                    iconImageSize: [56, 67],
                    iconImageOffset: [-5, -38]
                });
                wurth[15] = new ymaps.Placemark([61.764253, 34.443483], {
                    hintContent: 'г. Петрозаводск, ул. Онежской Флотилии, д. 22 ',
                    balloonContent: 'г. Петрозаводск, ул. Онежской Флотилии, д. 22 '
                }, {
                    iconLayout: 'default#image',
                    iconImageHref: 'img/map-marker.png',
                    iconImageSize: [56, 67],
                    iconImageOffset: [-5, -38]
                });
                wurth[16] = new ymaps.Placemark([57.810064, 28.336871], {
                    hintContent: 'г. Псков, Советская ул., д. 60 ',
                    balloonContent: 'г. Псков, Советская ул., д. 60 '
                }, {
                    iconLayout: 'default#image',
                    iconImageHref: 'img/map-marker.png',
                    iconImageSize: [56, 67],
                    iconImageOffset: [-5, -38]
                });
                wurth[17] = new ymaps.Placemark([61.655644, 50.831691], {
                    hintContent: 'г. Сыктывкар, Сысольское шоссе, д. 7/1 ',
                    balloonContent: 'г. Сыктывкар, Сысольское шоссе, д. 7/1 '
                }, {
                    iconLayout: 'default#image',
                    iconImageHref: 'img/map-marker.png',
                    iconImageSize: [56, 67],
                    iconImageOffset: [-5, -38]
                });
                wurth[18] = new ymaps.Placemark([59.126062, 37.906992], {
                    hintContent: 'г. Череповец, ул. Сталеваров, д. 46 ',
                    balloonContent: 'г. Череповец, ул. Сталеваров, д. 46 '
                }, {
                    iconLayout: 'default#image',
                    iconImageHref: 'img/map-marker.png',
                    iconImageSize: [56, 67],
                    iconImageOffset: [-5, -38]
                });

                myMap.behaviors.disable('scrollZoom');
                myMap.geoObjects
                    .add(wurth[1])
                    .add(wurth[2])
                    .add(wurth[3])
                    .add(wurth[4])
                    .add(wurth[5])
                    .add(wurth[6])
                    .add(wurth[7])
                    .add(wurth[8])
                    .add(wurth[9])
                    .add(wurth[10])
                    .add(wurth[11])
                    .add(wurth[12])
                    .add(wurth[13])
                    .add(wurth[14])
                    .add(wurth[15])
                    .add(wurth[16])
                    .add(wurth[17])
                    .add(wurth[18])
                ;
                $('.js-map').click(function () {
                    var mapId = parseInt($(this).data('map'));
                    myMap.setCenter(wurth[mapId].geometry.getCoordinates());
                    myMap.setZoom(14);
                    wurth[mapId].balloon.open();
                });
            });
        },

        smoothScroll: function () {
            var smoothScroll = new SmoothScroll('a[href*="#"]');
        },

        initCarousels: function () {
            $('.js-shop-carousel').slick({
                prevArrow: '<button id="prev" type="button" class="slick__btn slick__btn--prev"></button>',
                nextArrow: '<button id="next" type="button" class="slick__btn slick__btn--next"></button>'
            });
            $('.js-certs-carousel').slick({
                prevArrow: '<button id="prev" type="button" class="slick__btn slick__btn--prev"></button>',
                nextArrow: '<button id="next" type="button" class="slick__btn slick__btn--next"></button>'
            });
        },

        initMenu: function () {
            $('.js-header__menu-icon').click(function () {
                $('.js-header__menu').toggleClass('open');
            });
            $('.header__menu-close').click(function () {
                $('.js-header__menu').toggleClass('open');
            });
            $('.js-header__menu-link').click(function () {
                $('.js-header__menu').toggleClass('open');
            })
        },

        popupClose: function () {
        $('.js-mfp-close').click(function () {
            $.magnificPopup.close();
        });
    },
    }

})();

$(document).on('ready', mobile.init);






